package quarkus.books.api;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.access.annotation.Secured;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static io.restassured.RestAssured.given;

@Testcontainers
@QuarkusTest
public class BooksResourceTest {
    @Container
    public static MySQLContainer MYSQLDB = new MySQLContainer<>("mysql/mysql-server:latest")
            .withDatabaseName("lyncasdb")
            .withUsername("lyncas")
            .withPassword("pass-lyncas-user")
            .withExposedPorts(3307);

    @Secured("admin")
    @Test
    public void testGetBooks() {
        given()
          .when().get("/books")
          .then()
             .statusCode(200);
    }

}