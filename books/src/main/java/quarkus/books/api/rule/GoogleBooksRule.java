package quarkus.books.api.rule;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import quarkus.books.api.entity.BooksEntity;
import quarkus.books.api.entity.GoogleBooksEntity;
import quarkus.books.api.dto.VolumeDTO;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class GoogleBooks {
    private static final String URL_BOOKS = "https://www.googleapis.com/books/v1/volumes";

    public static void search() {
        String json = ClientBuilder.newClient().target(URL_BOOKS)
                .queryParam("q", "a")
                .queryParam("libraryRestrict","no-restrict")
                .queryParam("maxResults",40)
                .queryParam("startIndex",1)
                .request()
                .accept(MediaType.APPLICATION_JSON).get(String.class);

        JsonNode node;
        ObjectMapper objMapper = new ObjectMapper();

        VolumeDTO volumeDTO = new VolumeDTO();
        volumeDTO.kind = json;

        try {
            node = objMapper.readTree(json);
            volumeDTO = objMapper.treeToValue(node, VolumeDTO.class);
        } catch (Exception e) {
            volumeDTO.kind = e.getMessage();
            e.printStackTrace();
        }

        GoogleBooksEntity googleBooks;
        for(VolumeDTO.Items books : volumeDTO.items) {
            if(GoogleBooksEntity.find("google_id", books.id).count() == 0) {
                googleBooks = new GoogleBooksEntity();
                googleBooks.name = books.volumeInfo.title;
                googleBooks.google_id = books.id;
                googleBooks.favorite = BooksEntity.count("google_id", books.id) > 0 ? "YES" : "NOT";
                googleBooks.persist();
            }
        }
    }
}
