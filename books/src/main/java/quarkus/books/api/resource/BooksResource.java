package quarkus.books.api.resource;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.springframework.security.access.annotation.Secured;
import quarkus.books.api.Books;
import quarkus.books.api.dto.GoogleBooksDTO;
import quarkus.books.api.rule.GoogleBooks;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BooksResource {

    @Secured("admin")
    @GET
    public List<GoogleBooksDTO> listBooks() {
        return GoogleBooks.search();
    }

    @Secured("admin")
    @POST
    @Path("{id}/favorite")
    @Transactional
    public Response favoriteBook(@PathParam("id") String google_id) {
        try {
            PanacheQuery<Books> search_book = Books.find("google_id", google_id);
            Books book;

            if (search_book.count() == 0) {
                book = new Books();
                book.google_id = google_id;
                book.persist();
            }
            return Response.status(Response.Status.OK).build();
        } catch(Exception ex) {
            return Response.status(Response.Status.SEE_OTHER).build();
        }
    }

    @Secured("admin")
    @DELETE
    @Path("{id}/favorite")
    @Transactional
    public Response deleteFavoriteBook(@PathParam("id") String google_id) {
        try {
            PanacheQuery<Books> search_book = Books.find("google_id", google_id);

            if (search_book.count() > 0) {
                search_book.singleResult().delete();
                return Response.status(Response.Status.OK).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception exception) {
            return Response.status(Response.Status.SEE_OTHER).build();
        }
    }
}