package quarkus.books.api.resource;

import org.springframework.security.access.annotation.Secured;
import quarkus.books.api.Books;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/with-stars")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FavoriteResource {
    @Secured("admin")
    @GET
    public List<Books> listBooks() {
        return Books.findAll().list();
    }
}
