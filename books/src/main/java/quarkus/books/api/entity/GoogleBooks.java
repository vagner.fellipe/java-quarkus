package quarkus.books.api.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GoogleBooksEntity extends PanacheEntity {
    @Column(unique = true)
    public String google_id;

    public String name;

    public String favorite;
}
