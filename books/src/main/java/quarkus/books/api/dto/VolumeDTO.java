package quarkus.books.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeDTO {
    public String kind;
    public int totalItems;
    public List<Items> items;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Items {
        public String kind;
        public String id;
        public String etag;
        public String selfLink;
        public VolumeInfo volumeInfo;

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class VolumeInfo {
            public String title;
            public String subtitle;
        }
    }
}
